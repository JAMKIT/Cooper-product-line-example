# Projektituotantoympäristörunko

Tämä on projektituontantoympäristön esimerkki runko, jota voidaan käyttää ohjelmistotekniikan opetuksessa (Ainakin täällä JAMKilla :) )

Projektituotantoympäristö sisältää työkalut, esimerkki prosessin ja tarvittavat dokumenttipohjat alkavan projektitoiminnan tueksi.




* [GitHub-projektin etusivu](https://github.com/JAMK-IT/DOC10-example-project/wiki)
* [GitLab-projektin etusivu](https://gitlab.com/JAMKIT/Cooper-product-line-example/wikis/home)
* [Dokumenttipohjia](https://github.com/JAMK-IT/TTOS0800-Ohjelmistoprojekti/blob/master/pohja-projektin-etusivu.md)
* [Projektituotantolinja Cooper-asennusohje](https://github.com/JAMK-IT/DOC10-example-project/wiki/Cooper-product-line)
* [Prosessikäsikirja v0.0.1]()